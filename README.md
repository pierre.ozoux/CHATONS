Collectif d’Hébergeurs Alternatifs Transparents, Ouverts, Neutres et Solidaires (C.H.A.T.O.N.S.)
------------------------------------------------------------------------

Le collectif CHATONS vise à donner de la visibilité à des structures proposant des services en ligne et partageant une vision commune de l'hébergement.